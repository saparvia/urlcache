"""
Very simple hack to cache HTTP responses. Overwrites the standard HTTPHandler and HTTPSHandler to store results on the local filesystem. Useful during development of applications that request 3rd party data over the network. Because the standard handlers are overwritten also 3rd party libraries uusing urllib should cache the results automatically. The advantage over using an external caching proxy is that this method also works with HTTPS.

Written in 2016 by Stefan Parviainen <pafcu@iki.fi>

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
"""

"""
Example usage:
import urlcache
urlcache.cache.location = '/tmp' # Optional, writes tp /tmp by default

import urllib.request

# f = urllib.request.urlopen(...) # Just use the normal urllib functions
"""


import hashlib
import pickle
import logging
logger = logging.getLogger(__name__)

import urllib.request

# Make a copy of the original handlers
HTTPSHandler = urllib.request.HTTPSHandler
HTTPHandler = urllib.request.HTTPHandler

class Cache:
	def __init__(self, location='/tmp'):
		self.location = location

	def store(self, reply):
		"""Store contents of a HTTP reply in two files: one with the actual contents, and one with metadata"""

		contents = reply.read()
		reply.close()
		geturl = reply.geturl()

		logger.info('Caching %s', geturl)

		# Encode the URL so it's safe to use as a file name
		url_hash = hashlib.sha1(bytes(geturl,'utf-8')).hexdigest()
		with open('%s/cached_content_%s'%(self.location, url_hash), 'wb') as f:
			f.write(contents)


		meta = {
			'info': reply.info(),
			'geturl': reply.geturl(),
			'code': reply.code,
			'msg': reply.msg,
			'headers': reply.headers,
		}

		with open('%s/cached_meta_%s'%(self.location, url_hash), 'wb') as f:
			pickle.dump(meta, f)

		# Return the cached version
		return self.get(geturl)

	def get(self, url):
		"""Retrieve cached data"""
		logger.debug('Trying to get %s from cache', url)
		url_hash = hashlib.sha1(bytes(url,'utf-8')).hexdigest()
		contents_file = open('%s/cached_content_%s'%(self.location, url_hash), 'rb')

		with open('%s/cached_meta_%s'%(self.location, url_hash),'rb') as f:
			meta = pickle.load(f)

		info = lambda: meta['info']
		geturl = lambda: meta['geturl']

		# Add extra attributes to the contents file
		contents_file.info = info
		contents_file.geturl = geturl
		contents_file.headers = meta['headers']
		contents_file.code = meta['code']
		contents_file.msg = meta['msg']

		return contents_file

# Default cache
cache = Cache()

class CachingHttpsHandler(HTTPSHandler):
	def https_open(self, req):
		try:
			f = cache.get(req.get_full_url())
			logger.info('%s found in cache, so using that', req.get_full_url())
		except FileNotFoundError:
			logger.info('%s not found in cache, so retrieving it', req.get_full_url())
			f = super(CachingHttpsHandler, self).https_open(req)
			f = cache.store(f)
		return f

class CachingHttpHandler(HTTPHandler):
	def http_open(self, req):
		try:
			f = cache.get(req.get_full_url())
			logger.info('%s found in cache, so using that', req.get_full_url())
		except FileNotFoundError:
			logger.info('%s not found in cache, so retrieving it', req.get_full_url())
			f = super(CachingHttpHandler, self).http_open(req)
			f = cache.store(f)
		return f

urllib.request.HTTPSHandler = CachingHttpsHandler
urllib.request.HTTPHandler = CachingHttpHandler
